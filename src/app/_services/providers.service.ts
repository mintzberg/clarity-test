import { Injectable } from '@angular/core';
import { DATA } from '../_mock-data';

@Injectable()
export class ProvidersService {

  // TODO: http request
  providers = DATA;

  constructor() { }

  getProviders() {
    return this.providers;
  }

}
