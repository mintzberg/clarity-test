import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProvidersService } from './_services/providers.service'
import { Globals } from "./_globals";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
  providers: [ProvidersService]
})
export class AppComponent {

  mockData = [];
  // providerSelectedItems = [];
  // selectedFromSelectedItems = [];
  // allProvidersItemsNumber: number = 0;
  // allProvidersSelectedItemsNumber: number = 0;

    constructor(
      private router: Router,
      private providersService: ProvidersService,
      private globals: Globals) {
    }

    ngOnInit() {
      this.mockData = this.providersService.getProviders();
      // this.getProviderSelectedItems();
    }

  // getProviderSelectedItems() {
  //
  //   this.providerSelectedItems = [];
  //
  //   // Provider
  //   for (let provider = 0, provider_len = this.mockData.length; provider < provider_len; provider++) {
  //
  //     // Sport
  //     for (let sport = 0, sport_len = this.mockData[provider].sport.length; sport < sport_len; sport++) {
  //
  //       // League
  //       for (let league = 0, league_len = this.mockData[provider].sport[sport].league.length; league < league_len; league++) {
  //
  //         // Game
  //         for (let game = 0, game_len = this.mockData[provider].sport[sport].league[league].game.length; game < game_len; game++) {
  //
  //           if (this.mockData[provider].sport[sport].league[league].game[game].enable === true) {
  //             this.mockData[provider].itemSelected++;
  //             this.providerSelectedItems.push(this.mockData[provider].sport[sport].league[league].game[game]);
  //           }
  //         }
  //       }
  //     }
  //   }
  //
  //   return this.providerSelectedItems;
  //
  // }

  // getAllProvidersItemsNumber() {
  //
  //   this.allProvidersItemsNumber = 0;
  //
  //   // Provider
  //   for (let provider = 0, provider_len = this.mockData.length; provider < provider_len; provider++) {
  //
  //     // Sport
  //     for (let sport = 0, sport_len = this.mockData[provider].sport.length; sport < sport_len; sport++) {
  //
  //       // League
  //       for (let league = 0, league_len = this.mockData[provider].sport[sport].league.length; league < league_len; league++) {
  //
  //         // Game
  //         for (let game = 0, game_len = this.mockData[provider].sport[sport].league[league].game.length; game < game_len; game++) {
  //
  //           this.allProvidersItemsNumber++;
  //         }
  //       }
  //     }
  //   }
  //
  //   return this.allProvidersItemsNumber;
  //
  // }

  // getAllProvidersSelectedItemsNumber() {
  //
  //   this.allProvidersSelectedItemsNumber = 0;
  //
  //   // Provider
  //   for (let provider = 0, provider_len = this.mockData.length; provider < provider_len; provider++) {
  //
  //     // Sport
  //     for (let sport = 0, sport_len = this.mockData[provider].sport.length; sport < sport_len; sport++) {
  //
  //       // League
  //       for (let league = 0, league_len = this.mockData[provider].sport[sport].league.length; league < league_len; league++) {
  //
  //         // Game
  //         for (let game = 0, game_len = this.mockData[provider].sport[sport].league[league].game.length; game < game_len; game++) {
  //
  //           if (this.mockData[provider].sport[sport].league[league].game[game].enable === true) {
  //
  //             this.allProvidersSelectedItemsNumber++;
  //           }
  //         }
  //       }
  //     }
  //   }
  //
  //   return this.allProvidersSelectedItemsNumber;
  //
  // }
}
