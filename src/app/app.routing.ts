/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { Routes, RouterModule } from '@angular/router';

import { RisksComponent } from './risks/risks.component';
import { BetsComponent } from './bets/bets.component';
import { FeedComponent } from './feed/feed.component';


export const ROUTES: Routes = [
    {path: '', redirectTo: 'feed', pathMatch: 'full'},
    {path: 'feed', component: FeedComponent},
    {path: 'bets', component: BetsComponent},
    {path: 'risks', component: RisksComponent}
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES);
