import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  // transform(value: any, args?: any): any {
  //   return null;
  // }

  transform(providers, value) {

    return providers.filter(provider => {

      return provider.name.includes(value)

    })
  }

}
