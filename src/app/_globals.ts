import {Injectable} from '@angular/core';

@Injectable()
export class Globals {
  sideBar: string = 'sidenav--show';
  filterSelectedSport: string = '';
}
