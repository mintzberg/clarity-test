export const DATA: any[] = [
  {
    id: 1,
    name: 'Provider #1',
    itemSelected: 1,
    expanded: true,
    sport: [
      {
        id: 11,
        name: 'Basketball',
        expanded: true,
        league: [
          {
            id: 111,
            name: 'All Stars',
            expanded: true,
            game: [
              {
                id: 1111,
                name: 'TeamXX1 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
                enable: true,
              },
              {
                id: 1112,
                name: 'TeamXX2 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
              {
                id: 1113,
                name: 'TeamXX3 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
            ],
          },
          {
            id: 1112,
            name: 'NBA',
            expanded: true,
            game: [
              {
                id: 1121,
                name: 'TeamXX1 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
                enable: true,
              },
              {
                id: 1122,
                name: 'TeamXX2 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
              {
                id: 1123,
                name: 'TeamXX3 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
            ],
          }
        ],
      },
      {
        id: 12,
        name: 'Soccer',
        expanded: true,
        league: [
          {
            id: 121,
            name: 'Legends',
            expanded: true,
            game: [
              {
                id: 1211,
                name: 'TeamXX1 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
                enable: true,
              },
              {
                id: 1212,
                name: 'TeamXX2 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
            ],
          }
        ],
      },
    ],
  },
  {
    id: 2,
    name: 'Provider #2',
    sport: [
      {
        id: 21,
        name: 'Basketball',
        league: [
          {
            id: 211,
            name: 'League',
            game: [
              {
                id: 2111,
                name: 'TeamXX1 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
                enable: true,
              },
              {
                id: 2112,
                name: 'TeamXX2 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
              {
                id: 2113,
                name: 'TeamXX3 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
            ],
          }
        ],
      },
      {
        id: 22,
        name: 'Sport',
        league: [
          {
            id: 221,
            name: 'League',
            game: [
              {
                id: 2211,
                name: 'TeamXX1 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
                enable: false,
              },
              {
                id: 2212,
                name: 'TeamXX2 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
              {
                id: 2213,
                name: 'TeamXX3 - TeamXX',
                date: '2018-03-21',
                time: '09:00',
              },
            ],
          }
        ],
      },
    ],
  },
];
