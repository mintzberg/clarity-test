import {Component, Input, OnInit} from '@angular/core';
import { Globals } from '../../_globals';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() mockData;
  allProvidersSelectedItemsNumber = 0;

  constructor(private globals: Globals) {
  }

  ngOnInit() {
  }

  getAllProvidersSelectedItemsNumber() {

    this.allProvidersSelectedItemsNumber = 0;

    // Provider
    for (let provider = 0, provider_len = this.mockData.length; provider < provider_len; provider++) {

      // Sport
      for (let sport = 0, sport_len = this.mockData[provider].sport.length; sport < sport_len; sport++) {

        // League
        for (let league = 0, league_len = this.mockData[provider].sport[sport].league.length; league < league_len; league++) {

          // Game
          for (let game = 0, game_len = this.mockData[provider].sport[sport].league[league].game.length; game < game_len; game++) {

            if (this.mockData[provider].sport[sport].league[league].game[game].enable === true) {

              this.allProvidersSelectedItemsNumber++;
            }
          }
        }
      }
    }

    return this.allProvidersSelectedItemsNumber;

  }

  toggleSidenav() {

    if (this.globals.sideBar === 'sidenav--hide') {
      this.globals.sideBar = 'sidenav--show';
    } else {
      this.globals.sideBar = 'sidenav--hide';
    }
  }

}
