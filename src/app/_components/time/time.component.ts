import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss']
})
export class TimeComponent implements OnInit {

  public now: Date = new Date();
  public now1: string;
  public now2: string;

  constructor() {
    setInterval(() => {
      this.now = new Date();
      this.now1 = this.formatDate(this.now, 1);
      this.now2 = this.formatDate(this.now, 2);
    });
  }

  ngOnInit() {
  }

  formatDate(date, part = 1) {

    let year = date.getFullYear();
    let month = date.getMonth();
    let day = date.getDate();

    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();

    month = month + 1;
    if (month < 10) {
      month = '0' + month;
    }
    if (min < 10) {
      min = '0' + min;
    }
    if (sec < 10) {
      sec = '0' + sec;
    }

    if (part === 1) {
      return year + '-' + month + '-' + day;
    } else if (part === 2) {
      return hour + ':' + min + ':' + sec;
    } else {
      return year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;
    }
  }
}
