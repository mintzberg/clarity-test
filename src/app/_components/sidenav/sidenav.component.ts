import {Component, OnInit} from '@angular/core';
import { ProvidersService } from '../../_services/providers.service'

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  providers: [ProvidersService]
})
export class SidenavComponent implements OnInit {

  mockData = [];
  allProvidersSelectedItemsNumber: number = 0;

  constructor(private providersService: ProvidersService) { }

  ngOnInit() {
    this.mockData = this.providersService.getProviders();
    this.getAllProvidersSelectedItemsNumber();
  }

  getAllProvidersSelectedItemsNumber() {

    this.allProvidersSelectedItemsNumber = 0;

    // Provider
    for (let provider = 0, provider_len = this.mockData.length; provider < provider_len; provider++) {

      // Sport
      for (let sport = 0, sport_len = this.mockData[provider].sport.length; sport < sport_len; sport++) {

        // League
        for (let league = 0, league_len = this.mockData[provider].sport[sport].league.length; league < league_len; league++) {

          // Game
          for (let game = 0, game_len = this.mockData[provider].sport[sport].league[league].game.length; game < game_len; game++) {

            if (this.mockData[provider].sport[sport].league[league].game[game].enable === true) {

              this.allProvidersSelectedItemsNumber++;
            }
          }
        }
      }
    }

    return this.allProvidersSelectedItemsNumber;

  }

}
