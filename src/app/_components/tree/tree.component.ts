import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {

  @Input() mockData;
  searchStr = '';
  providerSelectedItemsNumber = 0;

  constructor() { }

  ngOnInit() {

    // console.log(this.mockData)

  }

  getProviderSelectedItemsNumber(i) {

    this.providerSelectedItemsNumber = 0;

    // Sport
    for (let sport = 0, sport_len = this.mockData[i].sport.length; sport < sport_len; sport++) {
      // League
      for (let league = 0, league_len = this.mockData[i].sport[sport].league.length; league < league_len; league++) {
        // Game
        for (let game = 0, game_len = this.mockData[i].sport[sport].league[league].game.length; game < game_len; game++) {

          if (this.mockData[i].sport[sport].league[league].game[game].enable === true) {
            this.providerSelectedItemsNumber++;
          }
        }
      }
    }

    return this.providerSelectedItemsNumber;

  }

}
