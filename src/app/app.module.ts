import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ClarityModule } from '@clr/angular';
import { AppComponent } from './app.component';
import { ROUTING } from "./app.routing";
import { FeedComponent } from "./feed/feed.component";
import { BetsComponent } from "./bets/bets.component";
import { TreeComponent } from './_components/tree/tree.component';
import { SearchPipe } from './_pipes/search.pipe';
import { RisksComponent } from './risks/risks.component';
import { SidenavComponent } from './_components/sidenav/sidenav.component';
import { HeaderComponent } from './_components/header/header.component';
import { TimeComponent } from './_components/time/time.component';
import { Globals } from './_globals';

@NgModule({
    declarations: [
        AppComponent,
        BetsComponent,
        FeedComponent,
        TreeComponent,
        SearchPipe,
        RisksComponent,
        SidenavComponent,
        HeaderComponent,
        TimeComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        ClarityModule,
        ROUTING
    ],
    providers: [Globals],
    bootstrap: [AppComponent]
})
export class AppModule {
}
