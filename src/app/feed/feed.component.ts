import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { ProvidersService} from "../_services/providers.service";
import { Globals } from '../_globals'

@Component({
  styleUrls: ['./feed.component.scss'],
  templateUrl: './feed.component.html',
  providers: [ProvidersService]
})
export class FeedComponent implements OnInit {

  mockData: any = [];
  selectedFromSelectedItems: any = [];
  providerSelectedItems: any = [];
  allProvidersSport: any = [];

  constructor(
    private router: Router,
    private providersService: ProvidersService,
    private globals: Globals
  ) {
  }

  ngOnInit() {
    this.mockData = this.providersService.getProviders();
    this.getProviderSelectedItems();
  }

  getProviderSelectedItems() {

    this.providerSelectedItems = [];
    this.allProvidersSport = [];

    // Provider
    for (let provider = 0, provider_len = this.mockData.length; provider < provider_len; provider++) {

      // Sport
      for (let sport = 0, sport_len = this.mockData[provider].sport.length; sport < sport_len; sport++) {

        // League
        for (let league = 0, league_len = this.mockData[provider].sport[sport].league.length; league < league_len; league++) {

          // Game
          for (let game = 0, game_len = this.mockData[provider].sport[sport].league[league].game.length; game < game_len; game++) {

            if (this.mockData[provider].sport[sport].league[league].game[game].enable === true) {

              this.mockData[provider].itemSelected++;

              let obj = this.mockData[provider].sport[sport].league[league].game[game];
              obj.provider_name = this.mockData[provider].name;
              obj.sport_name = this.mockData[provider].sport[sport].name;
              obj.league_name = this.mockData[provider].sport[sport].league[league].name;

              this.providerSelectedItems.push(obj);

              if (! this.allProvidersSport.includes(this.mockData[provider].sport[sport].name)) {
                this.allProvidersSport.push(this.mockData[provider].sport[sport].name);
              }
            }
          }
        }
      }
    }

    return this.providerSelectedItems;

  }

  onFilterClick(sport) {
    if (sport === this.globals.filterSelectedSport) {
      this.globals.filterSelectedSport = '';
    } else {
      this.globals.filterSelectedSport = sport;
    }
  }

  clickDelete(id) {
    // TODO: Remove this item from list
    console.log('Delete: ', id);
  }

}
